# BassTutor

## Contribution Process

We use Forking workflow. So:

1. Fork the repository.
2. Create a new branch for your feature or bug fix: `git checkout -b feature/your-feature-name`.
3. Make your changes, following the guidelines outlined above.
4. Commit your changes: `git commit -m "Your descriptive commit message"`.
5. Push to your branch: `git push origin feature/your-feature-name`.
6. Submit a pull request to the `main` branch of our repository, detailing the changes introduced by your contribution.

## Code Review

Once your pull request is submitted, it will undergo review by maintainers. They may provide feedback or request modifications before merging.

## Need Help?

If you have any questions or need clarification on any aspect of the contribution process, feel free to reach out to us via the project's issue tracker or other available communication channels.
