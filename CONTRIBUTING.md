# Contributing Guidelines

Thank you for considering contributing to our Python project! To maintain consistency and quality in our codebase, please follow these guidelines when contributing.

## Code Formatting

We use the following tools for code formatting:

- **Formatter**: Black
  - Parameters:
    - line-length = 88
    - skip-string-normalization = true

- **Import Sorter**: isort
  - Profile: Black

## Linter

We use Flake8 as our linter to ensure adherence to Python coding standards and detect potential issues in the codebase.

We appreciate your contributions and look forward to collaborating with you!
